# Purpose
This is my collection of gitignore templates for reference.

# Links
Here are some links to useful gitignore templates.

 * [Github gitignore](https://github.com/github/gitignore)
 * [gitignore.io](https://www.gitignore.io)

